<div class="left-nav">
    <div id="side-nav">
        <ul id="nav">
            <li>
                <a href="javascript:;">
                    <i class="iconfont">&#xe6b8;</i>
                    <cite>用户管理</cite>
                    <i class="iconfont nav_right">&#xe697;</i>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a _href="<?php echo e(url('admin/user')); ?>">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>用户列表</cite>
                        </a>
                    </li >
                    <li>
                        <a _href="<?php echo e(url('admin/user/create')); ?>">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>添加用户</cite>
                        </a>
                    </li >
                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="iconfont">&#xe6b8;</i>
                    <cite>角色管理</cite>
                    <i class="iconfont nav_right">&#xe697;</i>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a _href="<?php echo e(url('admin/role')); ?>">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>角色列表</cite>
                        </a>
                    </li >
                    <li>
                        <a _href="<?php echo e(url('admin/role/create')); ?>">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>添加角色</cite>
                        </a>
                    </li >
                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="iconfont">&#xe6b8;</i>
                    <cite>权限管理</cite>
                    <i class="iconfont nav_right">&#xe697;</i>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a _href="<?php echo e(url('admin/permission')); ?>">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>权限列表</cite>
                        </a>
                    </li >
                    <li>
                        <a _href="<?php echo e(url('admin/permission/create')); ?>">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>添加权限</cite>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="iconfont">&#xe6b8;</i>
                    <cite>分类管理</cite>
                    <i class="iconfont nav_right">&#xe697;</i>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a _href="<?php echo e(url('admin/cate')); ?>">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>分类列表</cite>
                        </a>
                    </li >
                    <li>
                        <a _href="<?php echo e(url('admin/cate/create')); ?>">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>添加分类</cite>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="iconfont">&#xe6b8;</i>
                    <cite>文章管理</cite>
                    <i class="iconfont nav_right">&#xe697;</i>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a _href="<?php echo e(url('admin/article')); ?>">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>文章列表</cite>
                        </a>
                    </li >
                    <li>
                        <a _href="<?php echo e(url('admin/article/create')); ?>">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>添加文章</cite>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="iconfont">&#xe723;</i>
                    <cite>网站配置管理</cite>
                    <i class="iconfont nav_right">&#xe697;</i>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a _href="<?php echo e(url('admin/config/create')); ?>">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>添加网站配置</cite>
                        </a>
                    </li >
                    <li>
                        <a _href="<?php echo e(url('admin/config')); ?>">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>网站配置列表</cite>
                        </a>
                    </li >
                </ul>
            </li>
            
                
                    
                    
                    
                
                
                    
                        
                            
                            
                        
                    
                    
                        
                            
                            
                        
                    
                    
                        
                            
                            
                        
                    
                    
                        
                            
                            
                        
                    
                
            
            
                
                    
                    
                    
                
                
                    
                        
                            
                            
                        
                    
                    
                        
                            
                            
                        
                    
                    
                        
                            
                            
                        
                    
                    
                        
                            
                            
                        
                    
                    
                        
                            
                            
                        
                    
                    
                        
                            
                            
                        
                    
                    
                        
                            
                            
                        
                    
                    
                        
                            
                            
                        
                    
                
            
        </ul>
    </div>
</div>
<!-- <div class="x-slide_left"></div> -->