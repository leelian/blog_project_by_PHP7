<!DOCTYPE html>
<html>
  
  <head>
    <meta charset="UTF-8">
    <title>用户授权页面</title>
    <meta name="renderer" content="webkit">
      <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
      <?php echo $__env->make('admin.public.styles', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo $__env->make('admin.public.script', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </head>
  
  <body>
    <div class="x-body">


        <form enctype="multipart/form-data" id="art_form" class="layui-form" action="<?php echo e(url('admin/user/doauth')); ?>" method="post">
          <?php echo e(csrf_field()); ?>


            <div class="layui-form-item">

                <label for="L_username" class="layui-form-label">
                    <span class="x-red">*</span>用户名称
                </label>
                <div class="layui-input-inline">
                    <input type="hidden" value="<?php echo e($user->user_id); ?>" name="user_id">
                    <input type="text" value="<?php echo e($user->user_name); ?>" disabled="" name="user_name" required="" lay-verify=""
                           autocomplete="off" class="layui-input">
                </div>
            </div>


            <div class="layui-form-item" pane="">
                <label class="layui-form-label">所有的角色</label>
                <div class="layui-input-block">
                    <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    
                    <?php if(in_array($v->id,$own_roleids)): ?>
                    <input type="checkbox" checked value="<?php echo e($v->id); ?>" name="role_id[]" lay-skin="primary" title="<?php echo e($v->role_name); ?>" >
                    <?php else: ?>
                    <input type="checkbox"  value="<?php echo e($v->id); ?>" name="role_id[]" lay-skin="primary" title="<?php echo e($v->role_name); ?>" >
                    <?php endif; ?>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>


          <div class="layui-form-item">
              <label for="L_repass" class="layui-form-label">
              </label>
              <button  class="layui-btn" lay-filter="add" lay-submit="">
                  授权
              </button>
          </div>
      </form>
    </div>
    <script>
        layui.use(['form','layer'], function(){
            $ = layui.jquery;
          var form = layui.form
          ,layer = layui.layer;
        


          //监听提交
          form.on('submit(add)', function(data){
            // return false;
          });
          
          
        });
    </script>
    <script>var _hmt = _hmt || []; (function() {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?b393d153aeb26b46e9431fabaf0f6190";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
      })();</script>
  </body>

</html>