<style>
 #main-wrap-left .span_1 a.fancyimg{
  width: 300px !important;
  padding: 0;
 }
 #main-wrap-left a.fancyimg {
  width: 260px !important;
  float: left!important;
  max-height: 230px;
  overflow: hidden;
 }
</style>
<?php $__env->startSection('main-wrap'); ?>
 <!-- Main Wrap -->
 <div id="main-wrap">
  <div id="sitenews-wrap" class="container"></div>
  <?php echo $__env->make('home.public.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <!-- Header Banner -->
  <!-- /.Header Banner -->
  <!-- CMS Layout -->
  <div class="container two-col-container cms-with-sidebar">
   <div id="main-wrap-left">
    <div class="bloglist-container clr">
     <?php $__currentLoopData = $arts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
     <article class="home-blog-entry col span_1 clr">
      <a href="<?php echo e(url('detail/'.$v->art_id)); ?>" title="<?php echo e($v->art_title); ?>" class="fancyimg home-blog-entry-thumb">
       <div class="thumb-img">
        <img src="<?php echo e($v->art_thumb); ?>" alt="<?php echo e($v->art_title); ?>" original="">
        <span><i class="fa fa-pencil"></i></span>
       </div> </a>
      <div class="home-blog-entry-text clr">
       <h3> <a href="<?php echo e(url('detail/'.$v->art_id)); ?>" title="<?php echo e($v->art_title); ?>" target="_blank"><?php echo e($v->art_title); ?></a> </h3>
       <!-- Post meta -->
       <div class="meta">
        <span class="postlist-meta-time"><i class="fa fa-calendar"></i>2周前 (09-26)</span>
        <span class="postlist-meta-views"><i class="fa fa-fire"></i>浏览: 3</span>
        <span class="postlist-meta-comments"><i class="fa fa-comments"></i><a href="https://www.lmonkey.com"><span>评论: </span>0</a></span>
       </div>
       <!-- /.Post meta -->
       <p><?php echo e($v->art_description); ?><a rel="nofollow" class="more-link" style="text-decoration:none;" href="<?php echo e(url('detail/'.$v->art_id)); ?>"></a></p>
      </div>
      <div class="clear"></div>
     </article>
     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
    <!-- pagination -->
    <div class="clear">
    </div>
    <div class="pagination">
     <?php echo e($arts->links()); ?>

    </div>
    <!-- /.pagination -->
   </div>
   ##parent-placeholder-4ea6d5a242c9a52f7a4c8c85c74f07675d76243d##
  </div>
  <div class="clear">
  </div>
  <!-- Blocks Layout -->
 </div>
 <!--/.Main Wrap -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.home', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>